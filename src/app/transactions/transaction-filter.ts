export class TransactionFilter {
    dateFrom: Date;
    dateTo: Date;
    asset: string;
}