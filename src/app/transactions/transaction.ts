export class Transaction {
    id: number;
    assetId: number;
    assetName: string;
    transactionTime: Date;
    transactionType: string;
    volume: number;
    price: number;
    balance: number;
}