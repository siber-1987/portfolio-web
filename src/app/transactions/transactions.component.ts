import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { TransactionsService } from '../transactions.service';
import { Transaction } from './transaction';
import { TransactionFilter } from './transaction-filter';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.less']
})
export class TransactionsComponent implements OnInit {

  transactions: Transaction[];
  filteredTransactions: Transaction[];
  filter: TransactionFilter;
  totalBalance: number = 0;

  assets: string[];
  filteredAssets: Observable<string[]>;
  assetControl = new FormControl();

  displayedColumns: string[] = ['assetName', 'transactionTime', 'transactionType', 'volume', 'price', 'balance'];

  constructor(private transactionsService: TransactionsService) { }

  ngOnInit() {
    this.getTransactions();
    this.filteredTransactions = this.transactions;
    this.filter = new TransactionFilter(); 
  }

  getTransactions(): void {
    this.transactionsService.getTransactions()
      .subscribe(transactions => {
        this.transactions = transactions;
        this.updateFilteredResult();
        this.updateAssetsList();

        this.filteredAssets = this.assetControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filterAssets(value))
          );
      } );
  }

  updateTotalBalance(): void {
    var sum = 0;
    this.filteredTransactions.forEach(item => {
      sum += item.balance;
    });
    this.totalBalance = sum;
  }

  updateFilteredResult(): void {
    var newFilteredTransactions: Transaction[] = [];

    for(var item of this.transactions)
    {
      //  dateFrom
      if (this.filter.dateFrom !== undefined && this.filter.dateFrom !== null) {
        if (item.transactionTime < this.filter.dateFrom) {
          continue;
        }
      }

      // dateTo
      if (this.filter.dateTo !== undefined && this.filter.dateTo !== null) {
        if (item.transactionTime > this.filter.dateTo) {
          continue;
        }
      }

      // asset
      if (this.filter.asset !== undefined && this.filter.asset !== null && this.filter.asset != '') {
        if (item.assetName != this.filter.asset) {
          continue;
        }
      }

      newFilteredTransactions.push(item);
    }

    this.filteredTransactions = newFilteredTransactions;
    this.updateTotalBalance();
  }

  updateAssetsList(): void {
    var newAssetsList: string[] = [];

    for(var item of this.transactions)
    {
      if (!newAssetsList.includes(item.assetName)){
        newAssetsList.push(item.assetName);
      }
    }

    this.assets = newAssetsList;
  }

  private _filterAssets(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.assets.filter(option => option.toLowerCase().includes(filterValue));
  }

  onDateFromChange(event): void {
    this.filter.dateFrom = event.value;
    this.updateFilteredResult();
  }

  onDateToChange(event): void {
    this.filter.dateTo = event.value;
    this.updateFilteredResult();
  }

  onAssetChange(event): void {
    this.filter.asset = event.option.value;
    this.updateFilteredResult();
  }

}
