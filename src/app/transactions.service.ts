import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from "rxjs/operators";
import { Transaction } from './transactions/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  private apiUrl = 'http://localhost:16023/api/transactions';

  constructor(private http: HttpClient) { }

  public getTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(this.apiUrl)
      .pipe(map(response => {
        response.forEach(item => {
          item.transactionTime = new Date(item.transactionTime)
          if (item.transactionType == '0') item.transactionType = 'Buy';
          else item.transactionType = "Sell";
        });
        return response;
      }));
  }
}
